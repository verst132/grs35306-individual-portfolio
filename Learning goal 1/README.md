## Learning goal 1: _To be able to easily search for publicly available data and increase my skills in assessing data quality_
_Category: Data_

### Background
Before the start of this course, I had very little experience with finding data for a project by myself. For pretty much every other course, the data for a project was directly provided beforehand. Whenever I did have to find data by myself, I found it difficult to find exactly what I was looking for. On top of that, I have little to no experience in critically assessing publicly available data in terms of quality, reliability and ethicality. These are the reasons why I decided to make this my first learning goal, as I believe that finding and using high quality data is one of the most important aspects of being a data scientist.

### Methodology
In order to accomplish this goal, I have written a short paper on how to go about finding publicly available data, as well as certain data assessment criteria that exist to ensure high quality. Next to this paper, I have compiled a list of all datasets we found during this course and describe how we found them. Additionally, I described which assessment criteria I used to ensure the quality of the found data.

### Details about the implementation
The paper was mostly written using scientific literature, but I also used Lucas Grus' lecture on data as a source. For the file describing the data we used for our project, I also included the data that I did not find by myself. I did ask my group members how they found said data and I assessed the quality of these datasets by myself. 

### Results
The results include a paper on finding and assessing data, which can be found in the file named "Paper_on_Finding_and_Assessing_Data.ipynb". The document where all data sources we used are listed, including information about how we went about finding them and an assessment of their quality can be found in the file "Data_sources_used_in_group_project.ipynb".

### Conclusions
The conclusion of the paper is that the search for relevant publicly available data involves navigating different data properties, ranging from formal to informal, structured to unstructured and intended to opportunistic data. These properties can help to find relevant data for a project. Assessing data quality is crucial, and dimensions like accuracy, completeness, consistency, and timeliness help with determining the quality of the data. On top of that, the ethical aspect of data should also not be ignored.
The conclusion of the file on the data used in our group project is that most of the data is of relatively high quality, with the exception of the proxies we used to estimate income levels in Delhi and the social media data gathered from TikTok videos. 

In terms of the accomplishment of my learning goal, I feel like I now know where to start when searching for freely available data when starting a new project. When I know what data properties I am looking for (formal vs informal, structured vs unstructured, intended vs opportunistic), it becomes much easier to actively search for data. Previously I would have just started looking for anything without planning out a certain route I want to take. After this course, I now also feel like I can assess the quality of data much better. I like that I now have something that I can always come back to when I want to know the quality of the data I found. 
