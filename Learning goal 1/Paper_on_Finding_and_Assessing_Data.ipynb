{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "984601d1",
   "metadata": {},
   "source": [
    "## **Finding and Assessing Public Data**\n",
    "\n",
    "### _**Paper on finding publicly available data & data quality assessment**_\n",
    "\n",
    "_**Tom Verstraten – GRS35306**_\n",
    "\n",
    "Looking for reliable, high quality data online can be quite challenging. In many cases, the perfect dataset for a certain topic does not exist, and you have to be creative in your search for data. In this short paper, I will discuss different ways to find publicly available data, and describe the ways in which the quality of the data can be assessed.\n",
    "\n",
    "When searching for data, there are certain properties of data that can help with finding what specific data you are looking for. One such data property is the formality of the data. When starting to look for data, it is often best to start looking for so-called 'formal data'. Formal data is provided by official institutes, where the terms of use are most often clearly defined (Jiang et al., 2018). The quality of the data is also likely to be very high when it is provided by an authoritative source. When searching for air quality data in Delhi for example, a formal data source would be the website of the Delhi government. There is however a chance that these formal sources do not have the data that you are specifically looking for. In that case, it is also possible to look for 'informal data'. In recent years, many citizens have started to contribute their own data to create high volume datasets (Jiang et al., 2018). Though this is often a very interesting source of data, it must be mentioned that the quality of said data is much harder to guarantee, and the terms of use are often not clearly defined. In the example of finding air quality data for Delhi, an informal source of data would be the data provided by personal weather stations from citizens.\n",
    "\n",
    "Apart from the formality of the data, it is also important to keep the structure of the data in mind when searching for the perfect dataset. A structured dataset is well-organized and easy to use, often in the form of a spreadsheet (Mishra & Misra, 2017). Although it is easy to use, structured data is often relatively one-sided, meaning that it does not always provide the exact data you need (Grus, 2024). Even though structured data is easier to use, most of the data that can be found on the internet is unstructured (Azad et al., 2019). Unstructured data basically consists of all data that is not provided in a pre-defined data format, think of images, documents or social media messages (Mishra & Misra, 2017). It is much more difficult to use unstructured data, but more often than not it opens up new opportunities to address different aspects of the problem (Grus, 2024). It is good to think of using unstructured data whenever the perfect structured dataset does not exist.\n",
    "\n",
    "When the data that you are looking for simply does not seem to exist, it is time to start looking for so-called 'opportunistic data'. In classical science, it is standard to look for 'intended data', which is data that is collected with the purpose of answering the research question (Grus, 2024). Sometimes, there is no such data to be found, in which case opportunistic data can be a way to still collect relevant data. Data can be called opportunistic when it has been collected without standardized protocols (Corradini et al., 2019). When it is impossible to find the economic status of each postcode in Delhi for example, you could use housing prices or the amount of electric vehicle charging stations as opportunistic data to estimate the mean wealth in each postcode. It should be noted that opportunistic data often lacks any metadata, and it is difficult to guarantee high quality results (Corradini et al., 2019).\n",
    "\n",
    "The previously mentioned data properties could help in finding the perfect data for a specific project. After finding the data, it is however important to critically assess its quality. Batini et al. (2009) have defined four different dimensions to assess data quality: accuracy, completeness, consistency and timeliness.\n",
    "\n",
    "Accuracy is the first dimension used to assess data quality. Different definitions for data accuracy exist in literature, but Batini et al. (2009) have distinguished two different types of accuracy: syntactic and semantic. However, only syntactic accuracy is relevant for assessing data quality, and it is defined as \"the closeness of a value to the elements of the corresponding definition domain\" (Batini et al., 2009, p.7). So basically, how close is the data to the real world? With social media data for example, there is something as a demographic bias, where the people posting messages on social media do not represent the demographic of the entire population (Pokhriyal et al., 2023). The accuracy of social media data should thus always be looked at with this bias in mind.\n",
    "\n",
    "Another dimension that is used to assess data quality is completeness. Batini et al. (2009) define 'completeness' as \"the degree to which a given data collection includes data describing the corresponding set of real-world objects\" (Batini et al., 2009, p. 7). A dataset is not complete when there are 'null values' involved. These are missing values in a dataset for a value that does exist in the real world (Batini et al., 2009). Therefore, if a dataset is less complete, that also means a lower quality rating.\n",
    "\n",
    "Consistency is the next dimension that Batini et al. (2009) mention for data quality assessment. The authors mention that data should be consistent in relation to certain integrity constraints, which in this case are rules ensuring that the values in the dataset consistently make sense (Batini et al., 2009). When you have a dataset with the daily temperature values in Delhi for example, and there are some values that exceed realistic values (such as \\>100°C), the data is not consistent.\n",
    "\n",
    "The final dimension that can be used to assess data quality according to Batini et al. (2009) is timeliness. In literature, there are many different definitions of timeliness, but the overall meaning seems to be the extent to which the age of the data is appropriate for the project (Wand & Wang, 1996; Batini et al., 2009). When you want to analyse the impact of vegetation on air quality in Delhi, it is thus important to use NDVI data and air quality data for the same time period to get the most high quality data.\n",
    "\n",
    "Aside from the quality of the data, it is also crucial to assess the ethical aspect of the data source. When working with publicly available data, it is important to stay within the lines of the terms of use established by the data provider, for example. Sometimes, there are more complex ethical dilemmas in the world of data science. The use of social media data is an example of such an ethically challenging practice, where the privacy of the subjects should be protected at all costs (Di Minin et al., 2021). In general, it would be best to steer clear of such ethical dilemmas, but such data sources like social media can open up countless new research opportunities. When working with such data, it is crucial to view the ethics of the situation as a priority.\n",
    "\n",
    "In conclusion, the search for relevant publicly available data involves navigating different data properties, ranging from formal to informal, structured to unstructured and intended to opportunistic data. Assessing data quality is crucial, and dimensions like accuracy, completeness, consistency, and timeliness help with determining the quality of the data. The ethical aspect of data should also not be ignored. Knowing how and where to find data, and how to assess data quality is essential when starting a data science project.\n",
    "\n",
    "**References**\n",
    "\n",
    "Azad, P., Navimipour, N. J., Rahmani, A. M., & Sharifi, A. (2019). The role of structured and unstructured data managing mechanisms in the Internet of things. _Cluster Computing_, _23_(2), 1185–1198. [https://doi.org/10.1007/s10586-019-02986-2](https://doi.org/10.1007/s10586-019-02986-2)\n",
    "\n",
    "Batini, C., Cappiello, C., Francalanci, C., & Maurino, A. (2009). Methodologies for data quality assessment and improvement. _ACM Computing Surveys_, _41_(3), 1–52. [https://doi.org/10.1145/1541880.1541883](https://doi.org/10.1145/1541880.1541883)\n",
    "\n",
    "Corradini, F., González, N., Casado, F., Rojas, V., & van der Ploeg, M. (2019). Usefulness of an opportunistic data analysis approach to evaluate if environmental regulations aim at relevant applications. Geoderma, 351, 261-269. [https://doi.org/10.1016/j.geoderma.2019.05.007](https://doi.org/10.1016/j.geoderma.2019.05.007)\n",
    "\n",
    "Di Minin, E., Fink, C., Hausmann, A., Kremer, J., & Kulkarni, R. (2021). How to address data privacy concerns when using social media data in conservation science. _Conservation Biology_, _35_(2), 437–446. [https://doi.org/10.1111/cobi.13708](https://doi.org/10.1111/cobi.13708)\n",
    "\n",
    "Grus, L. (2024). Data for Smart Environments [Lecture Slides; BrightSpace]. [https://brightspace.wur.nl/d2l/le/content/239381/viewContent/1106894/View](https://brightspace.wur.nl/d2l/le/content/239381/viewContent/1106894/View)\n",
    "\n",
    "Jiang, Q., Bregt, A., & Kooistra, L. (2018). Formal and informal environmental sensing data and integration potential: perceptions of citizens and experts. _Science of The Total Environment_, _619–620_, 1133–1142. [https://doi.org/10.1016/j.scitotenv.2017.10.329](https://doi.org/10.1016/j.scitotenv.2017.10.329)\n",
    "\n",
    "Mishra, S., & Misra, A. (2017). Structured and unstructured big data analytics. _2017 International Conference on Current Trends in Computer, Electrical, Electronics and Communication (CTCEEC)_. [https://doi.org/10.1109/ctceec.2017.8454999](https://doi.org/10.1109/ctceec.2017.8454999)\n",
    "\n",
    "Pokhriyal, N., Valentino, B. A., & Vosoughi, S. (2023). Quantifying participation biases on social media. _EPJ Data Science_, _12_(1). [https://doi.org/10.1140/epjds/s13688-023-00405-6](https://doi.org/10.1140/epjds/s13688-023-00405-6)\n",
    "\n",
    "Wand, Y., & Wang, R. Y. (1996). Anchoring data quality dimensions in ontological foundations. _CACM_, _39_(11), 86–95. [https://doi.org/10.1145/240455.240479](https://doi.org/10.1145/240455.240479)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
