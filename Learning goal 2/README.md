## Learning goal 2: _To be able to to apply natural language processing (NLP) on social media data_
_Category: Information_

### Background
Before the start of this course, I had basically no knowledge on natural language processing other than knowing that it exists. During the first week of the course, we were introduced to the concept a bit more, and I decided that this is something that I would like to know more about. It was quite difficult to find a way to incorporate natural language processing into our group project on the impact of vegetation on air quality and temperature in Delhi. In the end we decided to apply it in the form of a sentiment analysis on three different TikTok videos on the poor air quality in Delhi.

### Methodology
The sentiment analysis was performed in Python. The bert-base-multilingual-uncased model from Hugging Face was used as a pre-trained sentiment analysis model (source: https://huggingface.co/nlptown/bert-base-multilingual-uncased-sentiment). Some of the code was written with help of ChatGPT, but we tried to do as much as possible by ourselves. The used data consists of cleaned CSV files of the TikTok comments for each of the three videos, where all Personally Identifiable Information was deleted. These comments were scraped using the following tutorial: https://www.youtube.com/watch?v=FsQEm2zalWA. 

### Details about the implementation
The sentiment analysis in Python can be found in the Jupyter Notebook file in the directory of learning goal 2. The analysis was done in 12 different steps. If you want to run the code on your own computer, you might need to install some of the libraries beforehand and download the CSV files containing TikTok comments. To read these CSV files, the path in the code should also be changed to work on your computer.

### Results
The Jupyter Notebook which shows the code we used for the sentiment analysis has the following results:

- A CSV file showing the sentiment score of each comments for each video
- Two visualizations:
    - A bar chart showing the distribution of sentiment labels for each video
    - A word cloud based on the most frequent words in the TikTok comments for each video

These results can be found in the 'results and visualizations' directory.

### Conclusions
The conclusions that can be drawn based on the results of the sentiment analysis is that most of the comments on the TikTok videos about the poor air quality in Delhi have a negative sentiment. The word clouds do not really provide interesting insight in the sentiment of people on these videos, but they do show some other interesting things. For example, one of the most prominent words in the comments of video 1 is 'Canada', which is most likely due to the fact that Canada had recently introduced a controversial carbon law when this video came out. Many people in the comments appeared critical of this law and the fact that India has a much higher pollution rate without having to deal with such a carbon law. 

In terms of the accomplishment of my learning goal, I now know how to perform a sentiment analysis on a social media dataset using Python. Before the start of this project, it was completely unclear to me what natural language processing is and how it can be implemented, but now I have some experience with it. The most difficult part of the process was trying to figure out where to start, but in the end we managed to get results that we were happy with, even if they do not really fit in with the rest of the project. 
