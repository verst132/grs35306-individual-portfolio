{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "68644d61",
   "metadata": {},
   "source": [
    "# **Ethics of Public Data: Navigating Privacy in Social Media Analytics**\n",
    "\n",
    "## ***Paper on the ethical and privacy aspects of using social media data***\n",
    "\n",
    "***Tom Verstraten – GRS35306***\n",
    "\n",
    "Social media data has recently emerged as a very valuable tool for conducting extensive analytics on large-scale human activity (Martí et al., 2019). The recent surge in availability of social media data has opened up new opportunities for researchers to revolutionize data research (Golder et al., 2017). One such opportunity is the ability to perform large-scale sentiment analyses on thousands of people at once, using the messages people post on social media. Despite the exciting opportunities of using social media data for research, it does not come without ethical challenges (Metcalf & Crawford, 2016; Zimmer, 2018; Martí et al., 2019; Golder et al., 2017). \n",
    "\n",
    "One such ethical challenge is the privacy of the people whose social media activity is used for research. The implications of possible privacy violations can be severe, as the right to privacy of personal data is a fundamental right (Di Minin et al., 2021). Some researchers, however, believe that using social media data without taking privacy into account is acceptable. A famous example is the OKCupid dataset created by Kirkegaard & Kjerrekær (2016), where the authors mentioned that “all the data found in the dataset are or were already publicly available, so releasing the dataset merely presents it in a more useful form” (Kirkegaard & Kjerrekær, 2016, p.2). The dataset consisted of the profile information of many users of the online dating website, which included their username, age, gender, location and sexual orientation (Zimmer, 2018). While a lot of this information can be seen as harmless, Golder et al. (2017) mention that there are certain vulnerable groups that can be seriously harmed by the publication of such data. One example is the fact that homosexuality is illegal or frowned upon in certain regions or religious groups. If the sexual orientation of these vulnerable people were to be exposed, it could lead to dangerous situations (Golder et al., 2017). After all, it is quite easy to combine personal information such as location, age and race to determine the person to whom the data relates (Majeed et al., 2017).  In order to prevent such situations, the data should always be anonymized by removing Personally Identifiable Information (PII) (Majeed et al., 2017). \n",
    "\n",
    "Next to privacy concerns, there are also problems surrounding informed consent and the right of withdrawal (Zimmer, 2018; Zook et al., 2017; Metcalf & Crawford, 2016). According the Nuremberg Code from 1947, informed consent of the subjects is required for research, as well as the right to withdraw without any consequences (Metcalf & Crawford, 2016). Informed consent means that the participants of a study participate voluntarily and are informed of all the risks involved (Zimmer, 2018). When performing a large-scale study on social media comments, acquiring informed consent for each subject is virtually impossible, resulting in a severe ethical problem (Heise et al., 2019). To overcome this dilemma, researches most often opt for removing Personally Identifiable Information by pseudonymizing the data (Heise et al., 2019). The right to withdraw from a study is also difficult to guarantee in such large-scale social media studies. What makes this even more difficult is the fact that this right of withdrawal also applies to pseudonymized data (Di Minin et al., 2021). In literature, there is no clear solution for this problem. After all, a social media post is not automatically removed from a dataset when the user deletes it on the social platform.\n",
    "\n",
    "Aside from the ethical dilemmas regarding the people whose data is used in these types of studies, there are also ethical issues regarding the use of the data itself. The demographic bias in social media data, for example, can cause unintended consequences when the results of a study are used for decision-making (Pokhriyal et al., 2023; Wang et al, 2023). It is crucial to recognize that certain demographic groups are over- or underrepresented on social media. Generally, younger people are more active on social media compared to older people, and political opinions are more likely to be extreme (Magin, 2019). This means that if you want to use social media data to gather information about the general public opinion, you must recognize that is will most likely not be entirely representative of reality. Hence, the ethics of using such data for policy making should be considered, as it could lead to biased or skewed policy decisions. \n",
    "\n",
    "In conclusion, while social media data opens up exciting research possibilities, it raises some ethical concerns. Protecting the privacy of subjects is crucial, urging responsible practices such as removing Personally Identifiable Information. Obtaining informed consent in social media studies proves virtually impossible, and withdrawal rights remain complex. Additionally, demographic biases in social media data can skew results, posing ethical issues in policy decision making. Acknowledging these biases is very important for responsible data use. When analysing social media data, researchers must prioritize ethics to ensure a balance between innovation and protecting individual rights.\n",
    "\n",
    "**References**\n",
    "\n",
    "Di Minin, E., Fink, C., Hausmann, A., Kremer, J., & Kulkarni, R. (2021). How to address data privacy concerns when using social media data in conservation science. *Conservation Biology*, *35*(2), 437–446. <https://doi.org/10.1111/cobi.13708> \n",
    "\n",
    "Golder, S., Ahmed, S., Norman, G., & Booth, A. (2017). Attitudes Toward the Ethics of Research Using Social Media: A Systematic review. *Journal of Medical Internet Research*, *19*(6), e195. <https://doi.org/10.2196/jmir.7082> \n",
    "\n",
    "Heise, A. H. H., Hongladarom, S., Jobin, A., Kinder-Kurlanda, K., Sun, S., Lim, E. L., Markham, A., Reilly, P. J., Tiidenberg, K. & Wilhelm, C. (2019). Internet research: Ethical guidelines 3.0. <https://members.aoir.org/resources/Documents/IRE%203.0%20-%20final%20distribution%20copy%20for%20AoIR%20members.pdf> \n",
    "\n",
    "Kirkegaard, E. O. W. & Bjerrekær, J. D. (2016). *The OKCupid dataset: A very large public dataset of dating site users*. OpenPsych. <https://openpsych.net/paper/46/> \n",
    "\n",
    "Magin, M. (2019). *Why social media data don’t tell us very much about public opinion*. @realSocialMedia. <https://www.ntnu.no/blogger/realsocialmedia/2019/05/22/why-social-media-data-dont-tell-us-very-much-about-public-opinion/> \n",
    "\n",
    "Martí, P., Serrano-Estrada, L., & Nolasco‐Cirugeda, A. (2019). Social Media data: Challenges, opportunities and limitations in urban studies. *Computers, Environment and Urban Systems*, *74*, 161–174. <https://doi.org/10.1016/j.compenvurbsys.2018.11.001> \n",
    "\n",
    "Majeed, A., Ullah, F., & Lee, S. (2017). Vulnerability- and Diversity-Aware Anonymization of Personally Identifiable Information for Improving User Privacy and Utility of Publishing Data. Sensors, 17(5), 1059. <https://doi.org/10.3390/s17051059>.\n",
    "\n",
    "Metcalf, J., & Crawford, K. (2016). Where are human subjects in Big Data research? The emerging ethics divide. *Big Data & Society*, *3*(1), 205395171665021. <https://doi.org/10.1177/2053951716650211> \n",
    "\n",
    "Pokhriyal, N., Valentino, B. A., & Vosoughi, S. (2023). Quantifying participation biases on social media. *EPJ Data Science*, *12*(1). <https://doi.org/10.1140/epjds/s13688-023-00405-6> \n",
    "\n",
    "Wang, Y., & Singh, L. (2023). Mitigating demographic bias of machine learning models on social media. EAAMO '23: Proceedings of the 3rd ACM Conference on Equity and Access in Algorithms, Mechanisms, and Optimization, Article No.: 24, Pages 1–12. <https://doi.org/10.1145/3617694.3623244>.\n",
    "\n",
    "Zimmer, M. (2018). Addressing Conceptual Gaps in Big Data Research Ethics: An Application of Contextual Integrity. *Sage Journals*, *4*(2), 205630511876830. <https://doi.org/10.1177/2056305118768300> \n",
    "\n",
    "Zook, M., Barocas, S., Boyd, D., Crawford, K., Keller, E., Gangadharan, S. P., Goodman, A. A., Hollander, R. D., Koenig, B. A., Metcalf, J., Narayanan, A., Nelson, A., & Pasquale, F. A. (2017). Ten simple rules for responsible big data research. *PLOS Computational Biology*, *13*(3), e1005399. <https://doi.org/10.1371/journal.pcbi.1005399> \n",
    "\n",
    "\n",
    "\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
