## Learning goal 3: _Increase my knowledge about the ethical and privacy aspects of using data sources_
_Category: Knowledge_

### Background
Before the start of this course, I did not know a lot about the ethical and privacy aspects of using social media as a data source. During previous courses I learned about licences and copyright infringement, but not so much about ethics and privacy. I wanted to be better informed on this topic, as I believe it to be very important. 

### Methodology
In order to show my progress for this goal, I wrote a short paper (~850 words) using scientific literature on the ethic dilemmas surrounding the use of social media data in research.

### Details about the implementation
The paper was written by using scientific literature, most of which were found through Google Scholar and Scopus.

### Results
The paper can be found in the file "Paper_on_Ethics_of_Social_Media_Data.ipynb" in the directory of learning goal 3. 

### Conclusions
After reading scientific literature and writing the paper on the ethics of using social media data, I have been able to reach the following conclusions: 

- Protecting the privacy of subjects is crucial, urging responsible practices such as removing Personally Identifiable Information.
- Obtaining informed consent in social media studies proves virtually impossible, and withdrawal rights remain complex.
- Demographic biases in social media data can skew results, posing ethical issues in policy decision making.

In terms of the accomplishment of my learning goal, I believe I am now much more informed on the ethical side of social media data. Before this course, I had never really thought about having to remove Personally Identifiable Information from such data, even though it seems like it should be obvious. The question of how to cope with the problems surrounding informed consent and the right of withdrawal are quite difficult, and I am personally not sure how this should be fixed. Removing personal information should in most cases be enough to solve that problem, but there is still no official consent given by the original subjects to use their data. The demographic biases in social media data was not that new to me, as I have read about that before. In conclusion, I now know what I should take into account when working with social media in the future.
