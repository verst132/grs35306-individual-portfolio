# GRS35306 Individual Portfolio
## Tom Verstraten
#### _In this repository, you can find the following directories and their contents:_
- _Introduction_
	- Contains the original personal learning plan in the form of a Jupyter Notebook

- _Learning goal 1_
	- README file describing:
		- The background  
		- Methodology and data source used  
		- Details about the implementation  
		- Results  
		- Conclusions both on the results as well as on the accomplishment of the goal
	- A paper on finding and assessing data (as a Jupyter Notebook)
	- A file describing all data sources used in the group project, including information on how these data sources were found and an assessment of their quality

- _Learning goal 2_
	- README file describing: 
		- The background  
		- Methodology and data source used  
		- Details about the implementation  
		- Results  
		- Conclusions both on the results as well as on the accomplishment of the goal
	- Jupyter Notebook containing code for Natural Language Processing
	- A directory showing the results of the script for each TikTok video:
		- A CSV file showing the sentiment score of each comments for each video
		- A bar chart showing the distribution of sentiment labels for each video
		- A word cloud based on the most frequent words in the TikTok comments for each video

- _Learning goal 3_
	- README file describing: 
		- The background  
		- Methodology and data source used  
		- Details about the implementation  
		- Results  
		- Conclusions both on the results as well as on the accomplishment of the goal
	- A paper on the ethics of using social media data in research (as a Jupyter Notebook)
		
- _Reflection_
	- Contains a Jupyter Notebook in which the entire learning process is reflected upon

